﻿using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace OpenXMLTest.Controllers
{
    public class HomeController : Controller
    {
        [HttpPost]
        public ActionResult Excel()
        {
            var files = Request.Files;
            var file = Request.Files[0];

            var path = Path.Combine(Server.MapPath("~/App_Data/Excels_Test/"), file.FileName);
            file.SaveAs(path);
            var list = ReadExcelFileDOM(path);

            return View(list);
        }

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        static List<WtocArticle> ReadExcelFileDOM(string fileName)
        {
            using (SpreadsheetDocument spreadsheetDocument = SpreadsheetDocument.Open(fileName, false))
            {
                WorkbookPart workbookPart = spreadsheetDocument.WorkbookPart;
                WorksheetPart worksheetPart = workbookPart.WorksheetParts.First();
                SheetData sheetData = worksheetPart.Worksheet.Elements<SheetData>().First();
                string text = "";

                var articlesList = new List<WtocArticle>();

                foreach (Row r in sheetData.Elements<Row>())
                {
                    var cellValues = new List<string>();

                    foreach (Cell c in r.Elements<Cell>())
                    {
                        text = ReadExcelCell(c, workbookPart);
                        cellValues.Add(text);
                    }

                    var item = new WtocArticle()
                    {
                        StockNumber = cellValues[3],
                        Date = cellValues[2],
                        StateChange = cellValues[4],
                        PartnerName = cellValues[10],
                        AccountNumber = cellValues[8],
                        Packaging = cellValues[13]
                    };
                    articlesList.Add(item);

                }
                articlesList.RemoveRange(0, 2);

                var onlyPositiveArticlesList = articlesList.Where(i => Convert.ToInt64(Convert.ToDouble(i.StateChange.TrimEnd().Replace('.', ','))) > 0).ToList();
                onlyPositiveArticlesList.ForEach(i => i.TotalQuantity = Convert.ToInt32(Convert.ToDouble(i.StateChange.TrimEnd().Replace('.', ','))) * Convert.ToInt32(Convert.ToDouble(i.Packaging.TrimEnd().Replace('.', ','))));

                return onlyPositiveArticlesList;
            }
        }

        static string ReadExcelCell(Cell cell, WorkbookPart workbookPart)
        {
            var cellValue = cell.CellValue;
            var text = (cellValue == null) ? cell.InnerText : cellValue.Text;
            if ((cell.DataType != null) && (cell.DataType == CellValues.SharedString))
            {
                text = workbookPart.SharedStringTablePart.SharedStringTable
                    .Elements<SharedStringItem>().ElementAt(
                        Convert.ToInt32(cell.CellValue.Text)).InnerText;
            }

            return (text ?? string.Empty).Trim();
        }
    }

    public class WtocArticle
    {
        // Kataloski broj
        public string StockNumber { get; set; }
        // Promjena stanja
        public string StateChange { get; set; }
        // Broj racuna
        public string AccountNumber { get; set; }
        // Datum
        public string Date { get; set; }
        // ImeKupcaDobavljaca
        public string PartnerName { get; set; }
        // Pakiranje
        public string Packaging { get; set; }
        // Ukupna Količina
        public double TotalQuantity { get; set; }

    }


}